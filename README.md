# @oddlog/record

## Record exports

A record instance has the following attributes:

| Name | Type | Description |
| --- | --- | --- |
| `array` | `Array` | The raw parsed array. |
| `schemaVersion` | `Number` | The schema version of the record. |
| `typeKey` | `?String` | The key within the payload that describes the type. |
| `isCustomMeta` | `Boolean` | Whether the meta data is not the default (platform, host, pid) meta data; it is not if custom meta data got passed during logger creation. |
| `metaInput` | `?Array/Object` | The plain meta data as passed during logger creation. |
| `meta` | `?Array/Object` | The parsed meta object (`{platform,host,pid}`; if `!isCustomMeta`), the `metaInput` otherwise. |
| `name` | `String` | The logger name. |
| `date` | `Date` | The record date. |
| `level` | `Number` | The severity level of the record. |
| `levelName` | `String` | The string value of `level`. |
| `floorLevelName` | `String` | The `levelName`, if found; the floor index severity level name otherwise. |
| `source` | `?{file,row,col}` | The source location of the logging method call. |
| `sourceInput` | `?[file,row,col]` | The raw `source`. |
| `message` | `String` | The primary message of the record. |
| `payload` | `*` | The payload of the record. |
