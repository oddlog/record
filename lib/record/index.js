import RecordV3 from "./v3";

/*===================================================== Exports  =====================================================*/

export {create};

/*==================================================== Functions  ====================================================*/

function create(rawString) {
  const array = JSON.parse(rawString);
  const schemaVersion = array[0];
  switch (schemaVersion) {
    case 3:
      return new RecordV3(array);
    default:
      throw new Error("Record version " + schemaVersion + " not supported.");
  }
}
