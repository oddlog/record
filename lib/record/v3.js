import {escape} from "@oddlog/utils";
import {valueToName, valueToFloorName} from "@oddlog/levels";

/*
 * [
 *   {Number}  SCHEMA_VERSION,
 *   {?String} typeKey,
 *   {?Array|Object} meta,   // if array of size 3, it is assumed to be default [platform, host, pid]
 *   {String}  loggerName,
 *   {Number}  timestamp,
 *   {Number}  level,
 *   {?Array|Object}  source: [
 *     {String} file,
 *     {Number} row,
 *     {Number} col
 *   ],
 *   {?String} message,
 *   {?Object} [payload]
 * ]
 */

/*===================================================== Exports  =====================================================*/

export default RecordV3;

/*===================================================== Classes  =====================================================*/

function RecordV3(array) {
  const meta = array[2];
  const source = array[6];
  this.array = array;
  this.schemaVersion = array[0];
  this.typeKey = array[1];
  this.isCustomMeta = !(Array.isArray(meta) && meta.length === 3);
  this.meta = this.isCustomMeta ? meta : {platform: meta[0], host: meta[1], pid: meta[2]};
  this.metaInput = meta;
  this.name = array[3];
  this.date = new Date(array[4]);
  this.level = array[5];
  this.levelName = valueToName(this.level);
  this.floorLevelName = valueToFloorName(this.level);
  this.source = source && {file: source[0], row: source[1], col: source[2]};
  this.sourceInput = source;
  this.message = array[7];
  this.payload = array[8];
}

RecordV3.prototype.toString = function () {
  const meta = JSON.stringify(this.isCustomMeta || this.meta == null ? this.meta : [
    this.meta.platform, this.meta.host, this.meta.pid
  ]);
  return "[" +
      this.schemaVersion + "," +
      (this.typeKey ? "\"" + escape(this.typeKey) + "\"" : "null") + "," +
      meta + "," +
      "\"" + this.name + "\"," +
      this.date.getTime() + "," +
      this.level + "," +
      JSON.stringify(this.sourceInput) + "," +
      (this.message ? "\"" + escape(this.message) + "\"" : "null") + "," +
      JSON.stringify(this.payload) +
      "]";

};
