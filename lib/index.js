import Log from "./Log";
import {create as parse} from "./record/index";

const SCHEMA_VERSION = 3;

/*===================================================== Exports  =====================================================*/

export {SCHEMA_VERSION, parse};

export default Log;
