import flatstr from "flatstr";
import {valueToName} from "@oddlog/levels";
import {clone, escape} from "@oddlog/utils";

/*===================================================== Exports  =====================================================*/

export default Log;

/*===================================================== Classes  =====================================================*/

/**
 * Creates a new log entry instance.
 *
 * @param {Logger} [logger] The logger to whose scope the message shall be bound.
 * @param {Number} [date] The timestamp of the log.
 * @param {Number} [level] The level of the log.
 * @param {?Object} [source] The source location of the caller.
 * @param {Array} [args] Arguments to be interpreted on-demand by {@link Log#_analyzeArguments}.
 * @param {Boolean} [useLevel] Whether Transports are supposed to filter the log if its level does not match the
 *        transports level restrictions. This is used to force the logging of internal logs.
 * @constructor
 */
function Log(logger, date, level, source, args, useLevel) {
  this.logger = logger;
  this.level = level;
  this.date = date;
  this._args = args;
  this._source = source;
  this._useLevel = useLevel;
  this.__data = null; // cached analysis of message method arguments
  this.__msgStringRaw = null; // cached raw record string
  this.__msgStringSimple = null; // cached simple record string
  this.__msg = null; // cached primary message string (formatted)
  this.__pld = null; // cached merged payload
  this.__pldOwn = false; // whether ownership of payload was already gained
  this.__hasMsg = false; // does __msg exist (needed since the value might as well be undefined)
  this.__hasPld = false; // does __pld exist (needed since the value might as well be undefined)
}

/**
 * Ensures that the message payload is in logger ownership. Thus it's either been passed with ownPayload set to
 * `true`, or been shallow cloned.
 */
Log.prototype.gainPayloadOwnership = function () {
  if (this.__pldOwn) { return; }
  this.__pldOwn = true;
  let data = this._analyzeArguments();
  if (!data.ownPayload) {
    data.ownPayload = true;
    if (data.payload != null) { data.payload = clone(data.payload); }
  }
};

/**
 * Returns the (cached) formatted primary message.
 *
 * @returns {String|undefined} The formatted primary message.
 */
Log.prototype.getFormattedMessage = function () {
  if (this.__hasMsg) { return this.__msg; }
  this.__hasMsg = true;
  let data = this._analyzeArguments();
  if (data.hasOwnProperty("message")) {
    if (data.hasOwnProperty("messageFormatting")) {
      return this.__msg = Reflect.apply(this.logger.messageFormatter, null, data.messageFormatting);
    }
    return this.__msg = data.message;
  }
};

/**
 * Returns the merged payload.
 *
 * @returns {?Object|undefined} The merged payload.
 */
Log.prototype.getPayload = function () {
  if (this.__hasPld) { return this.__pld; }
  this.__hasPld = true;
  let logger = this.logger, data = this._analyzeArguments();
  let payload = logger._getFinalPayload(logger.getPreparedPayload(), data.payload, data.ownPayload);
  data.ownPayload = true;
  return this.__pld = payload;
};

/**
 * Returns a string representation of the message object.
 *
 * @returns {String} The string representation of the message object.
 */
Log.prototype.getRawRecordString = function () {
  if (this.__msgStringRaw != null) { return this.__msgStringRaw; }
  let logger = this.logger, src = this._source, msg = this.getFormattedMessage(), payload = this.getPayload();
  let string = logger._prefix + logger.scope._prefix + this.date + "," + this.level + ",";
  string += src == null ? "null," : "[\"" + escape(src.file) + "\"," + src.row + "," + src.col + "],";
  string += msg == null ? "null" : "\"" + escape(msg) + "\"";
  string += payload !== void 0 ? "," + JSON.stringify(payload) + "]" : "]";
  return this.__msgStringRaw = flatstr(string);
};

/**
 * Returns a human readable simple string representation of the message object.
 *
 * @returns {String} The human readable simple string representation of the message object (without payload).
 */
Log.prototype.getSimpleRecordString = function () {
  if (this.__msgStringSimple != null) { return this.__msgStringSimple; }
  let string = "[" + new Date(this.date).toISOString() + "] ";
  string += valueToName(this.level).toUpperCase() + " ";
  string += this.logger.scope.name + ": ";
  string += this.getFormattedMessage();
  return this.__msgStringSimple = flatstr(string);
};

/**
 * Returns a string representation of the message object according to the provided format.
 *
 * @param {String|Function} format The format to use. Available choices are `"raw"` and `"simple"` or a function that
 *        formats the passed message.
 * @returns {String} The formatted message.
 */
Log.prototype.getRecordString = function (format) {
  if (typeof format === "function") { return format(this); }
  switch (format) {
    case "simple":
      return this.getSimpleRecordString();
    case "raw":
      return this.getRawRecordString();
    default:
      throw new Error("Record format '" + format + "' not supported.");
  }
};

/**
 * Analyzes the `args` array to distinguish parameters passed to logging methods of {@link Logger}.
 * The array is processed as following:
 * <ul>
 *   <li>If the current entry is a boolean, interpret as `ownPayload` option.</li>
 *   <li>If the current entry is an object, interpret as `payload`.</li>
 *   <li>If the current entry is a string, interpret as message and all remaining entries as formatting arguments.
 *       Otherwise throw.</li>
 * </ul>
 *
 * @returns {{ownPayload: (Boolean), payload: (?Object|undefined), message: (String|undefined)}} The resulting object.
 * @private
 */
Log.prototype._analyzeArguments = function () {
  if (this.__data != null) { return this.__data; }
  let args = this._args;
  let result = {ownPayload: this.logger.ownChildPayloads};
  let current = args[0], i = 0;
  if (typeof current === "boolean") {
    result.ownPayload = current;
    current = args[++i];
  }
  if (typeof current === "object") {
    result.payload = current;
    current = args[++i];
  }
  let _len = args.length;
  if (i < _len) {
    if (typeof current !== "string") { throw new Error("Expected message to be a String; Got " + typeof current); }
    result.message = current;
    if (i < _len - 1) {
      result.messageFormatting = args.slice(i); // contains message AND formatting parameters
    }
  }
  return this.__data = result;
};
